package TrafficSimulator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.dom.GenericDOMImplementation;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import TrafficSim.Simulator;

/**
 * Trida TrafficMain zajistuje parsovani parametru (cisla scenare) a spusti
 * simulator dopravy se zadanym scenarem. Pokud cislo scenare neni zadano
 * program se spusti s vychozim scenarem (scenar cislo 0)
 * 
 * @author Tomas Linhart
 * @version 1.0
 */
public class TrafficMain {

	/**
	 * Doba mezi jednotlivými překresleními simulace
	 */
	private static final int TIMER_PERIOD = 1000 / 100;

	private static double speedCoeficient = 1.0;

	private static long intervalStart;

	private static Timer timer;

	private static long timerStopMillis;

	private static TrafficWindow panel;
	
	private static JFrame frame;

	/**
	 * Trida zajisti vytvoreni okna (JFrame), panelu ve kterem se simulator dopravy
	 * bude vykreslovat (JPanel) a spusti timer, ktery bude simulator periodicky
	 * aktualizovat. Dale nacte cislo scenare z parametru a simulator spusti s danym
	 * parametrem. Vychozi cislo scenare je 0 - tento scenar se spusti pokud neni
	 * zadano cislo scenare, nebo je zadano cislo scenare, ktery v simulatoru
	 * neexistuje.
	 * 
	 * @param args parametry prikazove radky - cislo scenare
	 */
	public static void main(String[] args) {
		Simulator sim = new Simulator();
		String[] scenarios = sim.getScenarios();
		
		int scenarioNumber = 0;
		if (args.length == 1) {
			scenarioNumber = Integer.parseInt(args[0]);
		}

		if (scenarioNumber >= scenarios.length) {
			scenarioNumber = 0;
		}

		frame = new JFrame();
		frame.setLayout(new BorderLayout());

		try {
			panel = new TrafficWindow();
		} catch (IOException e1) {
			System.out.println("Nepodařilo se načíst obrázky!");
			e1.printStackTrace();
			return;
		}

		panel.setSim(sim);
		panel.setScenario(scenarios[scenarioNumber]);

		panel.setBackground(Color.WHITE);
		panel.setPreferredSize(new Dimension(1000, 800));
		frame.add(panel, BorderLayout.CENTER);

		createControls(frame);

		frame.setTitle("Traffic Simulator");
		frame.pack();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null); // Center na stred obrazovky

		frame.setVisible(true);

		intervalStart = System.currentTimeMillis();
		timer = new Timer(TIMER_PERIOD, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				long currentMillis = System.currentTimeMillis();
				panel.nextStep((currentMillis - intervalStart) * speedCoeficient);
				panel.repaint();
				intervalStart = currentMillis;
			}
		});

		timer.start();
	}

	/**
	 * Vrati hlavni frame
	 * @return hlavni okno
	 */
	public static JFrame getFrame() {
		return frame;
	}

	/**
	 * Vytvori ovladaci prvky v zadanem framu
	 * @param parentFrame vychozi frame
	 */
	public static void createControls(JFrame parentFrame) {
		int CONTROL_ELEMENT_HEIGHT = 25;
		// Panel s ovl�dac�mi prvky
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(0, 3, 10, 5));
		//controlPanel.setPreferredSize(new Dimension(300, CONTROL_ELEMENT_HEIGHT));
		controlPanel.setBackground(Color.WHITE);

		parentFrame.add(controlPanel, BorderLayout.NORTH);

		// Ovl�d�n� rychlosti
		JPanel speedPanel = new JPanel();
		speedPanel.setLayout(new BorderLayout());
		// Lev� offset
		speedPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

		JLabel speedLabel = new JLabel("Rychlost animace:");
		speedLabel.setPreferredSize(new Dimension(125, CONTROL_ELEMENT_HEIGHT));
		speedPanel.add(speedLabel, BorderLayout.WEST);

		JTextField numberInput = new JTextField();
		numberInput.setText("1.0");
		numberInput.setPreferredSize(new Dimension(50, CONTROL_ELEMENT_HEIGHT));
		speedPanel.add(numberInput, BorderLayout.CENTER);

		JButton confirmButton = new JButton("Potvrdit");
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String input = numberInput.getText();

				try {
					double inputParsed = Double.parseDouble(input);
					speedCoeficient = inputParsed;
				} catch (NumberFormatException ex) {
					System.out.println("Chybně zadaný parametr!");
					/**
					 * TODO? Vypsat do programu?
					 */
				}
			}
		});

		confirmButton.setPreferredSize(new Dimension(80, CONTROL_ELEMENT_HEIGHT));
		speedPanel.add(confirmButton, BorderLayout.EAST);

		// Pauzovac� tla��tko
		JButton pauseButton = new JButton("Pauza");
		pauseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pauseButton.getText().equals("Pauza")) {
					pauseButton.setText("Pokračovat");
					stopTimer();
				} else {
					pauseButton.setText("Pauza");
					startTimer();
				}
			}
		});

		pauseButton.setPreferredSize(new Dimension(10, CONTROL_ELEMENT_HEIGHT));

		JButton exportJPG = new JButton("Export do JPG");
		exportJPG.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stopTimer();
				
				Object[] data = getFileMetadata();	
				if (data != null) {
					JDialog dialog = createDialog("Probíhá export");
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							int width = (int) data[0];
							int height = (int) data[1];
							String fileName = (String) data[2];
							
							BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
							Graphics2D g = img.createGraphics();
							g.setColor(Color.WHITE);
							g.fillRect(0, 0, width, height);
							
							panel.paintTraffic(g, width, height);
							
							try {
								new File("img").mkdirs();
								File file = new File(fileName + ".jpg");
								ImageIO.write(img, "jpg", file);
								
								dialog.dispose();
								JOptionPane.showMessageDialog(null, "Export úspěšný. Uloženo do: " + file.getAbsolutePath());
								startTimer();
							} catch (IOException e1) {
								e1.printStackTrace();
								dialog.dispose();
								startTimer();
							}
						}
					});
					

					dialog.setVisible(true);
				} else {
					startTimer();
				}
			}
		});
		
		JButton exportSVG = new JButton("Export do SVG");
		exportSVG.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stopTimer();
				
				Object[] data = getFileMetadata();	
				if (data != null) {
					JDialog dialog = createDialog("Probíhá export");
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							int width = (int) data[0];
							int height = (int) data[1];
							String fileName = (String) data[2];
							
							DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();
							
							String svgNS = "http://www.w3.org/2000/svg";
							Document document = domImpl.createDocument(svgNS, "svg", null);
									
							SVGGraphics2D svgGenerator = new SVGGraphics2D(document);
							Graphics2D g = (Graphics2D)svgGenerator;

							g.setColor(Color.WHITE);
							g.fillRect(0, 0, width, height);
							
							panel.paintTraffic(g, width, height);

							
							try {
								new File("img").mkdirs();
								File file = new File(fileName + ".svg");
								
								svgGenerator.stream(new FileWriter(fileName + ".svg"), true);
								JOptionPane.showMessageDialog(null, "Export úspěšný. Uloženo do: " + file.getAbsolutePath());
								dialog.dispose();
								startTimer();
							} catch (IOException e1) {
								e1.printStackTrace();
								dialog.dispose();
								startTimer();
							}
						}
					});
					
					dialog.setVisible(true);
				} else {
					startTimer();
				}
			}
		});
		
		JButton printButton = new JButton("Tisk");
		printButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stopTimer();
				PrinterJob job = PrinterJob.getPrinterJob();
				job.setPrintable(panel);
				
				boolean doPrint = job.printDialog();
				if (doPrint) {
					try {
						job.print();
						JOptionPane.showMessageDialog(null, "Tisk úspěšný.");
						startTimer();
					} catch (PrinterException ex) {
						JOptionPane.showMessageDialog(null, "Chyba tisku!");
						startTimer();
					}
				} else {
					startTimer();
				}
			}
		});
		
		JButton laneColorButton = new JButton("Obarvení počtem vozidel");
		laneColorButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panel.getColoringMode() == Coloring.SPEED) {
					panel.setColoringMode(Coloring.COUNT);
					laneColorButton.setText("Obarvení rychlostí");
				} else {
					panel.setColoringMode(Coloring.SPEED);
					laneColorButton.setText("Obarvení počtem vozidel");
				}
			}
		});
		
		JButton zoomIn = new JButton("Přiblížit");
		JButton zoomOut = new JButton("Oddálit");
		
		zoomIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.zoomIn();
				if (panel.getZoomScale() == 5) {
					zoomIn.setEnabled(false);
				}
				zoomOut.setEnabled(true);
			}
		});
		
		zoomOut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.zoomOut();
				if (panel.getZoomScale() == 1) {
					zoomOut.setEnabled(false);
				}
				zoomIn.setEnabled(true);
			}
		});
		
		JButton centerButton = new JButton("Zarovnat na střed");
		centerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel.centerPan();
			}
		});
		
		

		controlPanel.add(speedPanel);
		controlPanel.add(laneColorButton);
		controlPanel.add(pauseButton);
		controlPanel.add(exportJPG);
		controlPanel.add(exportSVG);
		controlPanel.add(printButton);
		controlPanel.add(zoomIn);
		controlPanel.add(zoomOut);
		controlPanel.add(centerButton);
		controlPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.BLACK));

	}
	
	/**
	 * Zastaví časovač simulace
	 */
	public static void stopTimer() {
		timer.stop();
		timerStopMillis = System.currentTimeMillis();
	}
	
	/**
	 * Spustí časovač simulace
	 */
	public static void startTimer() {
		intervalStart = System.currentTimeMillis() - (timerStopMillis - intervalStart);
		timer.start();
	}
	
	/**
	 * Vytvoří dialog pro získání dat o výšce, šířce a názvu souboru
	 * @return získaná data
	 */
	private static Object[] getFileMetadata() {
		JTextField widthField = new JTextField();
		JTextField heightField = new JTextField();
		JTextField fileNameField = new JTextField();
		Object[] mess = new Object[] {
				"Šířka: ", widthField,
				"Výška: ", heightField,
				"Název souboru: ", fileNameField
		};
		
		int option = JOptionPane.showConfirmDialog(null, mess, "Export JPG", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
			int width = Integer.parseInt(widthField.getText());
			int height = Integer.parseInt(heightField.getText());
			String fileName = "img/" + fileNameField.getText();
			
			return new Object[] {width, height, fileName};
		} else {
			return null;
		}		
	}
	
	/**
	 * Vytvoří informativní dialog se zadanou zprávou
	 * @param message Informativní zpráva
	 * @return Instance dialogu
	 */
	private static JDialog createDialog(String message) {
		JOptionPane jop = new JOptionPane();
		jop.setMessageType(JOptionPane.INFORMATION_MESSAGE);
		jop.setMessage(message);
		JDialog dialog = jop.createDialog(null, message);
		
		return dialog;
	}
}
