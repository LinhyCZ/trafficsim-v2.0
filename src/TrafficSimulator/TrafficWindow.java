package TrafficSimulator;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import TrafficSim.Car;
import TrafficSim.CrossRoad;
import TrafficSim.Direction;
import TrafficSim.EndPoint;
import TrafficSim.Lane;
import TrafficSim.RoadSegment;
import TrafficSim.Simulator;
import TrafficSim.TrafficLight;
import TrafficSim.TrafficLightState;

/**
 * enum pro mody obarvovani jizdnich pruhu
 * @author Tomas Linhart
 *
 */
enum Coloring {
	SPEED, COUNT
}

/**
 * Trida TrafficSimulator zajistuje ziskani a vykresleni dat z knihovny
 * TrafficSim
 * 
 * @author Tomas Linhart
 * @version 1.0
 */

public class TrafficWindow extends JPanel implements Printable {
	private static final long serialVersionUID = -1344180800449385194L;
	/** Cas simulace v ms od spusteni */
	private long simulationTime = 0;

	/** Cas v ms kdy probehlo predchozi mereni pro statisticke ucely */
	private long meassurementPrevStep = 0;

	/** Instance simulatoru */
	private Simulator sim;

	/** Vychozi bod affini transfromace */
	private AffineTransform defaultTransform;

	/**
	 * Maximalni rychlost ve vsech jizdnich pruzich, slouzi pro vykresleni barvy
	 * jizdniho pruhu
	 */
	private double maxSpeedLimit = 0;

	/**
	 * Maximalni pocet aktualne projizdejicich vozidel, slouzi pro vykresleni barvy
	 * jizdniho pruhu
	 */
	private double maxCarCount = 0;

	/** Maximalni souradnice x daneho scenare */
	private double scenarioMaxX = Integer.MIN_VALUE;

	/** Maximalni souradnice y daneho scenare */
	private double scenarioMaxY = Integer.MIN_VALUE;

	/** Minimalni souradnice x daneho scenare */
	private double scenarioMinX = Integer.MAX_VALUE;

	/** Minimalni souradnice y daneho scenare */
	private double scenarioMinY = Integer.MAX_VALUE;

	/** Aktualni mod vybarvovani jizdniho pruhu */
	private Coloring coloringMode = Coloring.SPEED;

	/** Vyska aktualniho grafickeho kontextu */
	private double graphicsHeight = 0;

	/** Sirka aktualniho grafickeho kontextu */
	private double graphicsWidth = 0;

	/** Mapa obsahujici graficke objekty pro jednotlive jizdni pruhy */
	private Map<Lane, Path2D> laneGeometry = new HashMap<>();

	/** Mapa obsahujici graficke objekty pro jednotliva auta */
	private Map<Car, Shape> carGeometry = new HashMap<>();

	/** Mapa obsahujici affini transformace jednotlivych vozidel */
	private Map<Car, AffineTransform> carGeometryTransform = new HashMap<>();

	/** Mapa obsahujici graficke objekty pro jednotlive semafory */
	private Map<TrafficLight, Rectangle2D> trafficLightGeometry = new HashMap<>();

	/** Mapa obsahujici affini transformace jednotlivych semaforu */
	private Map<TrafficLight, AffineTransform> trafficLightGeometryTransform = new HashMap<>();

	/** Mapa obsahujici krizovatky u danych semaforu */
	private Map<TrafficLight, CrossRoad> trafficLightCrossroad = new HashMap<>();

	/** Seznam obsahujici aktualne zvyraznena auta */
	private ArrayList<Car> clickedCars = new ArrayList<>();

	/** Mapa obsahujici cas mereni prujezdu vozidel v danem jizdnim pruhu */
	private Map<Lane, ArrayList<Long>> passedCarsTime = new HashMap<>();

	/** Mapa obsahujici aktualni pocet prujezdu vozidel v danem jizdnim pruhu */
	private Map<Lane, ArrayList<Integer>> passedCarsCurrent = new HashMap<>();

	/** Mapa obsahujici celkovy pocet prujezdu vozidel v danem jizdnim pruhu */
	private Map<Lane, ArrayList<Integer>> passedCarsTotal = new HashMap<>();

	/** Mapa obsahujici cas mereni rychlosti vozidel */
	private Map<Car, ArrayList<Long>> carSpeedTime = new HashMap<>();

	/** Mapa obsahujici rychlosti vozidel */
	private Map<Car, ArrayList<java.lang.Double>> carSpeed = new HashMap<>();

	/** Meritko velikosti scenare vzhledem k velikosti okna */
	private double scaleCoeficient = 1.0;

	/** Uloziste bitmap obrazku semaforu */
	private Map<Direction, Map<TrafficLightState, BufferedImage>> trafficLightImageRepository = new HashMap<>();

	/** Posunuti na ose x */
	private double panX = 0;

	/** Posunuti na ose y */
	private double panY = 0;

	/** Nove posunuti na ose x */
	private double newPanX = 0;

	/** Nove posunuti na ose y */
	private double newPanY = 0;

	/** Promenna pro nacteni novych hodnot pri spusteni simulace */
	private boolean setMaxValues = false;

	/** Maximalni velikost scenare na ose x */
	private double maxX = 0;

	/** Maximalni velikost scenare na ose y */
	private double maxY = 0;

	/** Minimalni velikost scenare na ose x */
	private double minX = 0;

	/** Minimalni velikost scenare na ose y */
	private double minY = 0;

	/** Aktualni index meritka */
	private int zoomScale = 2;

	/** Meritka zoomu */
	private double[] zoomScales = new double[] { 0.5, 1, 2, 4, 8 };

	/** Vychozi bod kliknuti pro pan */
	private Point2D panStartPoint;

	// KONSTRUKTOR A FUNKCE PO VYTVORENI INSTANCE
	/**
	 * Konstruktor ktery nacte listenery pro kliknuti a pohyb mysi, dale nacte
	 * obrazky semaforu do pameti
	 * 
	 * @throws IOException Vyjimka v pripade neexistujiciho souboru
	 */
	public TrafficWindow() throws IOException {
		this.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				boolean skip = false;

				if (!skip) {
					for (Car car : carGeometry.keySet()) {
						Shape line = carGeometry.get(car);

						try {
							Point2D tp = null;
							tp = carGeometryTransform.get(car).inverseTransform(e.getPoint(), tp);

							if (line.contains(tp)) {
								if (e.getButton() == MouseEvent.BUTTON3) {
									if (clickedCars.contains(car)) {
										clickedCars.remove(clickedCars.indexOf(car));
									} else {
										clickedCars.add(car);
									}
									
									repaint();
								} else if (e.getButton() == MouseEvent.BUTTON1) {
									generateChart(car);
								}

								skip = true;
								break;
							}

						} catch (NoninvertibleTransformException e1) {
							e1.printStackTrace();
						}
					}
				}

				if (!skip) {
					for (Lane lane : laneGeometry.keySet()) {
						Path2D line = laneGeometry.get(lane);
						if (line.contains(new Point2D.Double(e.getX(), e.getY()))) {
							generateChart(lane);
							skip = true;
							break;
						}
					}
				}

				if (!skip) {
					for (TrafficLight light : trafficLightGeometry.keySet()) {
						Rectangle2D line = trafficLightGeometry.get(light);

						try {
							Point2D tp = null;
							tp = trafficLightGeometryTransform.get(light).inverseTransform(e.getPoint(), tp);

							if (line.contains(tp)) {
								JFrame timeTableFrame = new JFrame();

								TrafficTimeTable panel = new TrafficTimeTable(light, trafficLightCrossroad.get(light));

								panel.setPreferredSize(new Dimension(panel.calculateWidth(), panel.calculateHeight()));
								panel.setBackground(Color.WHITE);
								timeTableFrame.add(panel);

								timeTableFrame.setTitle("Traffic Simulator - TimeTable");
								timeTableFrame.pack();

								timeTableFrame.setLocationRelativeTo(null); // Center na stred obrazovky

								timeTableFrame.setVisible(true);

								skip = true;
								break;
							}

						} catch (NoninvertibleTransformException e1) {
							e1.printStackTrace();
						}
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				panStartPoint = new Point2D.Double(e.getPoint().getX(), e.getPoint().getY());
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				panX += newPanX;
				panY += newPanY;
				newPanX = 0;
				newPanY = 0;
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		this.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				newPanX = panStartPoint.getX() - e.getX();
				newPanY = panStartPoint.getY() - e.getY();
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		loadTrafficLights();
	}

	/**
	 * Nacte obrazky semaforu do pameti
	 * 
	 * @throws IOException
	 */
	void loadTrafficLights() throws IOException {
		Direction[] directions = new Direction[] { Direction.Left, Direction.Opposite, Direction.Right };
		TrafficLightState[] states = new TrafficLightState[] { TrafficLightState.GO, TrafficLightState.PREPARE_TO_GO,
				TrafficLightState.PREPARE_TO_STOP, TrafficLightState.STOP };
		for (int i = 0; i < directions.length; i++) {
			this.trafficLightImageRepository.put(directions[i], new HashMap<>());
			for (int j = 0; j < states.length; j++) {
				String imagePath = "res/" + directions[i] + states[j] + ".png";
				BufferedImage image = ImageIO.read(new File(imagePath));

				this.trafficLightImageRepository.get(directions[i]).put(states[j], image);
			}
		}
	}

	// VYPOCETNI FUNKCE
	/**
	 * Vypocte minimalni a maximalni hranici daneho scenare a ulozi do promennych
	 */
	void computeModelDimensions() {
		RoadSegment[] roads = sim.getRoadSegments();
		for (RoadSegment road : roads) {
			Point2D roadStartPoint = road.getStartPosition();
			Point2D roadEndPoint = road.getEndPosition();

			double laneOffset = road.getLaneSeparatorWidth()
					+ (road.getBackwardLanesCount() + road.getForwardLanesCount()) * road.getLaneWidth();

			if (isGoingXAxis(roadStartPoint, roadEndPoint)) {
				this.scenarioMaxX = (int) Math.max(roadStartPoint.getX() + laneOffset,
						Math.max(roadEndPoint.getX() + laneOffset, this.scenarioMaxX));
				this.scenarioMaxY = (int) Math.max(roadStartPoint.getY(),
						Math.max(roadEndPoint.getY(), this.scenarioMaxY));
				this.scenarioMinX = (int) Math.min(roadStartPoint.getX() - laneOffset,
						Math.min(roadEndPoint.getX() - laneOffset, this.scenarioMinX));
				this.scenarioMinY = (int) Math.min(roadStartPoint.getY(),
						Math.min(roadEndPoint.getY(), this.scenarioMinY));
			} else {
				this.scenarioMaxX = (int) Math.max(roadStartPoint.getX(),
						Math.max(roadEndPoint.getX(), this.scenarioMaxX));
				this.scenarioMaxY = (int) Math.max(roadStartPoint.getY() + laneOffset,
						Math.max(roadEndPoint.getY() + laneOffset, this.scenarioMaxY));
				this.scenarioMinX = (int) Math.min(roadStartPoint.getX(),
						Math.min(roadEndPoint.getX(), this.scenarioMinX));
				this.scenarioMinY = (int) Math.min(roadStartPoint.getY() - laneOffset,
						Math.min(roadEndPoint.getY() - laneOffset, this.scenarioMinY));

			}
		}
	}

	/**
	 * Vypise true, pokud se silnice pohybuje po x-ove ose.
	 * 
	 * @param start Pocatecni bod silnice
	 * @param end   Koncovy bod silnice
	 */
	boolean isGoingXAxis(Point2D start, Point2D end) {
		if (start.getX() == end.getX()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Vypocte konstantu pro prevod metru na pixely podle velikosti okna
	 * 
	 * @param width  sirka okna
	 * @param height vyska okna
	 */
	void computeModel2WindowTransformation(double width, double height) {
		double scaleX = width / (this.scenarioMaxX - this.scenarioMinX);
		double scaleY = height / (this.scenarioMaxY - this.scenarioMinY);

		this.scaleCoeficient = Math.min(scaleX, scaleY);
	}

	/**
	 * Vypocte pozici bodu v pixelech z bodu zadaneho v metrech pomoci konstanty
	 * 
	 * @param m Bod se souradnicemi v metrech
	 * @return Bod se souradnicemi v pixelech
	 */
	Point2D model2window(Point2D m) {
		double xR = m.getX() - this.scenarioMinX;
		double yR = m.getY() - this.scenarioMinY;

		double xO = xR * this.scaleCoeficient;
		double yO = yR * this.scaleCoeficient;

		yO = this.graphicsHeight - yO;

		int offsetX = (int) ((this.graphicsWidth - maxX + minX) / 2);
		int offsetY = (int) ((this.graphicsHeight - maxY + minY) / 2);

		xO = (xO - this.panX - this.newPanX + offsetX) * zoomScales[zoomScale - 1];
		yO = (yO - this.panY - this.newPanY - offsetY) * zoomScales[zoomScale - 1];

		return new Point2D.Double(xO, yO);
	}

	/**
	 * Vypocte delku usecky v pixelech z delky zadane v metrech pomoci konstanty
	 * 
	 * @param m delka usecky v metrech
	 * @return delka usecky v pixelech
	 */
	double model2window(double m) {
		return (int) (m * this.scaleCoeficient * zoomScales[zoomScale - 1]);
	}

	/**
	 * Vrati pole s jednotkovym normalovym vektorem se souradnicemi x a y
	 * 
	 * @param start vychozi bod
	 * @param end   koncovy bod
	 * @return pole s normalovym vektorem
	 */
	double[] getNormalVector(Point2D start, Point2D end) {
		double[] heading = getHeadingVector(start, end);
		double nx1 = 0;
		double ny1 = 0;
		nx1 = -heading[1];
		ny1 = heading[0];

		return new double[] { nx1, ny1 };
	}

	/**
	 * Vrati pole s jednotkovym smerovym vektorem
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	double[] getHeadingVector(Point2D start, Point2D end) {
		double ux = start.getX() - end.getX();
		double uy = start.getY() - end.getY();

		double delkaVektoru = getLength(ux, uy);
		double ux1 = ux / delkaVektoru;
		double uy1 = uy / delkaVektoru;

		return new double[] { ux1, uy1 };
	}

	/**
	 * Vrati vzdalenost mezi dvema body
	 * 
	 * @param start pocatecni bod
	 * @param end   koncovy body
	 * @return Vzdalenost mezi body
	 */
	double getLength(Point2D start, Point2D end) {
		double ux = start.getX() - end.getX();
		double uy = start.getY() - end.getY();

		return getLength(ux, uy);
	}

	/**
	 * Vrati delku smeroveho vektoru
	 * 
	 * @param ux x souradnice smeroveho vektoru
	 * @param uy y souradnice smeroveho vektoru
	 * @return delka smeroveho vektoru
	 */
	double getLength(double ux, double uy) {
		return Math.hypot(ux, uy);
	}

	/**
	 * Ziska uhel ze smeroveho vektoru vzhledem k ose x
	 * 
	 * @param vector Souradnice smeroveho vektoru
	 * @return uhel[rad] vzhledem k ose x
	 */
	double getAngle(double[] vector) {
		double ux1 = 1;
		double uy1 = 0;

		double ux2 = vector[0];
		double uy2 = vector[1];

		double dot = ux1 * uy2 - uy1 * uy2;
		double det = ux1 * ux2 + uy1 * uy2;

		return Math.atan2(det, dot);
	}

	// FUNKCE GRAFU

	/**
	 * Vygeneruje a zobrazi graf pro zadany objekt
	 * 
	 * @param key Instance auta nebo jizdniho pruhu
	 */
	public void generateChart(Object key) {
		JFrame graphFrame = new JFrame();
		graphFrame.setLayout(new BorderLayout());

		ChartPanel drawingArea = null;

		if (key instanceof Lane) {
			drawingArea = new ChartPanel(getCarCountChart((Lane) key));
		}

		if (key instanceof Car) {
			drawingArea = new ChartPanel(getCarSpeedChart((Car) key));
		}

		if (drawingArea == null)
			return;

		final ChartPanel printArea = drawingArea;
		drawingArea.setPreferredSize(new Dimension(700, 500));

		JButton printButton = new JButton("Tisk");
		printButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				TrafficMain.stopTimer();
				printArea.createChartPrintJob();
				TrafficMain.startTimer();
			}
		});

		graphFrame.add(drawingArea, BorderLayout.CENTER);
		graphFrame.add(printButton, BorderLayout.SOUTH);

		graphFrame.setTitle("Traffic Simulator - Graph");
		graphFrame.pack(); // Automaticky nastav� ���ku a v��ku podle obsahu okna
		graphFrame.setLocationRelativeTo(null); // Center na stred obrazovky
		graphFrame.setVisible(true);
	}

	/**
	 * Vygeneruje graf obsahujici poctu prujezdu vozidel v danem jizdnim pruhu
	 * 
	 * @param car Instance jizdniho pruhu
	 * @return Instance grafu prujezdu jidzniho pruhu
	 */
	JFreeChart getCarCountChart(Lane lane) {
		XYSeries data1 = new XYSeries("Aktualní auta");
		XYSeries data2 = new XYSeries("Celková auta");

		int index = passedCarsTime.get(lane).size();

		for (int i = 0; i < index; i++) {
			long time = passedCarsTime.get(lane).get(i);
			int countCurrent = passedCarsCurrent.get(lane).get(i);
			int countTotal = passedCarsTotal.get(lane).get(i);

			data1.add(time / 1000.0, countCurrent);
			data2.add(time / 1000.0, countTotal);
		}

		XYSeriesCollection dataSet = new XYSeriesCollection();
		dataSet.addSeries(data1);
		dataSet.addSeries(data2);

		JFreeChart chart = ChartFactory.createXYLineChart("Počet průjezdů vozidel", "čas [s]", "počet vozidel",
				dataSet);

		setChartStyle(chart);

		return chart;
	}

	/**
	 * Vygeneruje graf obsahujici rychlosti daneho auta
	 * 
	 * @param car Instance auta
	 * @return Instance grafu rychlosti auta
	 */
	JFreeChart getCarSpeedChart(Car car) {
		XYSeries data1 = new XYSeries("Rychlost auta");

		int index = carSpeedTime.get(car).size();

		for (int i = 0; i < index; i++) {
			long time = carSpeedTime.get(car).get(i);
			double currCarSpeed = carSpeed.get(car).get(i);

			data1.add(time / 1000.0, currCarSpeed);
		}

		XYSeriesCollection dataSet = new XYSeriesCollection();
		dataSet.addSeries(data1);

		JFreeChart chart = ChartFactory.createXYLineChart("Rychlost vybraného vozidla", "čas [s]", "rychlost vozidla",
				dataSet);

		return chart;
	}

	/**
	 * Nastavi styly zadaneho grafu
	 * @param chart Graf pro ktery se bude nastavovat styl
	 */
	void setChartStyle(JFreeChart chart) {
		chart.setBackgroundPaint(Color.WHITE);

		XYPlot plot = (XYPlot) chart.getPlot();
		Stroke stroke = new BasicStroke(0.3f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 9 }, 0);

		plot.setDomainGridlineStroke(stroke);
		plot.setDomainGridlinePaint(Color.BLACK);
		plot.setRangeGridlineStroke(stroke);
		plot.setRangeGridlinePaint(Color.BLACK);
		plot.setBackgroundPaint(Color.WHITE);
	}

	// VYKRESLOVACI FUNKCE
	/**
	 * Vykresli auto na zadnem miste, pokud je auto kratsi nez 4 metry, vytvori tvar
	 * obdelnika, pokud je mezi 4 a 6 metry, vytvori tvar elipsy, pokud je delsi
	 * vytvori tvar trojuhelnika
	 * 
	 * @param car   Instance auta ktere je vykreslovano
	 * @param width sirka auta v pixelech
	 * @param g     graficky kontext
	 * @return tvar vozidla
	 */
	Shape drawCar(Car car, double width, Graphics2D g) {
		Point2D position = this.model2window(car.getPosition());
		double orientation = car.getOrientation();
		double carLength = car.getLength();
		double length = this.model2window(car.getLength());

		g.setColor(getCarColor(car));
		g.translate(position.getX(), position.getY());

		g.rotate((Math.PI / 2) - orientation);

		Shape carShape;

		if (carLength <= 4.0) {
			carShape = new Rectangle2D.Double(0 - width / 2, 0, width, length);
		} else if (carLength <= 6.0) {
			carShape = new Ellipse2D.Double(0 - width / 2, 0, width, length);
		} else {
			double[] pointsY = new double[] { 0, length, length, 0 };
			double[] pointsX = new double[] { 0, 0 - width / 2, width / 2, 0 };
			Path2D carPath = new Path2D.Double();
			carPath.moveTo(pointsX[0], pointsY[0]);
			for (int i = 1; i < pointsY.length; i++) {
				carPath.lineTo(pointsX[i], pointsY[i]);
			}

			carShape = carPath;
		}

		g.fill(carShape);

		if (clickedCars.contains(car)) {
			drawText(car, g, (Math.PI / 2) - orientation);
		}

		return carShape;
	}

	/**
	 * Vykresli text obsahujici rychlost vozidla
	 * 
	 * @param car    Instance vozidla
	 * @param g      Graficky kontext
	 * @param rotate Orientace automobilu
	 */
	void drawText(Car car, Graphics2D g, double rotate) {
		AffineTransform tr = g.getTransform();

		g.rotate((2 * Math.PI) - rotate);
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 13));
		String text = String.format("%.2f km/h", car.getCurrentSpeed());
		FontMetrics metrics = g.getFontMetrics(g.getFont());
		int textHeight = metrics.getHeight();
		int textWidth = metrics.stringWidth(text);
		Rectangle2D background = new Rectangle2D.Double(0 - (textWidth / 2) - 5, 0 - textHeight * 2 + 3, textWidth + 10,
				textHeight);
		g.setColor(Color.WHITE);
		g.fill(background);
		g.setColor(Color.BLACK);
		g.draw(background);
		g.drawString(text, 0 - textWidth / 2, 0 - textHeight);
		g.setTransform(tr);
	}

	/**
	 * Vykresli jeden pruh silnice
	 * 
	 * @param start Zacatek pruhu v pixelech
	 * @param end   Konec pruhu v pixelech
	 * @param size  sirka pruhu v pixelech
	 * @param g     graficky kontext
	 * @return Polygon silnice
	 */
	Path2D drawLane(Point2D start, Point2D end, double size, Graphics2D g) {
		double[] normalVector = getNormalVector(start, end);
		double normalVectorX = normalVector[0];
		double normalVectorY = normalVector[1];

		double x1 = start.getX() - (size * normalVectorX) / 2;
		double y1 = start.getY() - (size * normalVectorY) / 2;
		double x2 = start.getX() + (size * normalVectorX) / 2;
		double y2 = start.getY() + (size * normalVectorY) / 2;
		double x3 = end.getX() + (size * normalVectorX) / 2;
		double y3 = end.getY() + (size * normalVectorY) / 2;
		double x4 = end.getX() - (size * normalVectorX) / 2;
		double y4 = end.getY() - (size * normalVectorY) / 2;

		double[] pointsX = new double[] { x1, x2, x3, x4 };
		double[] pointsY = new double[] { y1, y2, y3, y4 };
		
		Path2D lanePath = new Path2D.Double();
		lanePath.moveTo(pointsX[0], pointsY[0]);
		for (int i = 1; i < pointsY.length; i++) {
			lanePath.lineTo(pointsX[i], pointsY[i]);
		}

		g.fill(lanePath);
		return lanePath;
	}

	/**
	 * Vykresli jeden segment silnice
	 * 
	 * @param road Segment silnice, ktery se ma vykreslit
	 * @param g    graficky kontext
	 */
	void drawRoadSegment(RoadSegment road, Graphics2D g) {
		double laneSeparatorWidth = road.getLaneSeparatorWidth();
		double laneWidth = road.getLaneWidth();

		Point2D startPoint = road.getStartPosition();
		Point2D endPoint = road.getEndPosition();

		double[] normalVector = this.getNormalVector(startPoint, endPoint);

		for (int i = 0; i < road.getForwardLanesCount() + road.getBackwardLanesCount(); i++) {
			Lane lane;
			int index = i - road.getForwardLanesCount() + 1;
			double offset;
			int sgn = 1;
			if (i < road.getForwardLanesCount()) {
				offset = i * (laneWidth);
				sgn = 1;
				lane = road.getLane(i + 1);

			} else {
				offset = (index * laneWidth) + laneSeparatorWidth;
				sgn = -1;
				lane = road.getLane(index * -1);
			}

			Point2D startPointWithOffset = new Point2D.Double(startPoint.getX() + sgn * (offset * normalVector[0]),
					startPoint.getY() + sgn * (offset * normalVector[1]));
			Point2D endPointWithOffset = new Point2D.Double(endPoint.getX() + sgn * (offset * normalVector[0]),
					endPoint.getY() + sgn * (offset * normalVector[1]));

			g.setColor(getLaneColor(lane));
			Path2D line = this.drawLane(this.model2window(startPointWithOffset), this.model2window(endPointWithOffset),
					this.model2window(laneWidth), g);
			this.laneGeometry.put(lane, line);
		}
	}

	/**
	 * Vykresli krizovatku
	 * 
	 * @param cr Krizovatka silnic
	 * @param g  graficky kontext
	 */
	void drawCrossRoad(CrossRoad cr, Graphics2D g) {
		RoadSegment[] roads = cr.getRoads();
		EndPoint[] ends = cr.getEndPoints();

		List<Point2D> endPoints = new ArrayList<>();
		for (int i = 0; i < roads.length; i++) {
			if (roads[i] == null)
				continue;
			endPoints.add(roads[i].getEndPointPosition(ends[i]));
		}

		Lane[] lanes = cr.getLanes();

		for (int i = 0; i < lanes.length; i++) {
			RoadSegment endRoad = lanes[i].getEndRoad();
			RoadSegment startRoad = lanes[i].getStartRoad();
			int endLaneNumber = lanes[i].getEndLaneNumber();
			int startLaneNumber = lanes[i].getStartLaneNumber();

			g.setColor(getLaneColor(lanes[i]));

			Path2D endPoly = this.laneGeometry.get(endRoad.getLane(endLaneNumber));
			Path2D startPoly = this.laneGeometry.get(startRoad.getLane(startLaneNumber));

			int arrayIndex;
			if (endPoints.contains(endRoad.getStartPosition())) {
				arrayIndex = 0;
			} else {
				arrayIndex = 2;
			}
			
			double[] coords = new double[2];
			PathIterator iterator = endPoly.getPathIterator(null);
			for (int j = 0; j < arrayIndex; j++) iterator.next();
			iterator.currentSegment(coords);
			double x1 = coords[0];
			double y1 = coords[1];
			iterator.next();
			iterator.currentSegment(coords);
			double x2 = coords[0];
			double y2 = coords[1];

			if (endPoints.contains(startRoad.getStartPosition())) {
				arrayIndex = 0;
			} else {
				arrayIndex = 2;
			}
			
			iterator = startPoly.getPathIterator(null);
			for (int j = 0; j < arrayIndex; j++) iterator.next();
			iterator.currentSegment(coords);
			
			double x3 = coords[0];
			double y3 = coords[1];
			iterator.next();
			iterator.currentSegment(coords);
			double x4 = coords[0];
			double y4 = coords[1];

			double[] pointsX = new double[] { x1, x2, x3, x4 };
			double[] pointsY = new double[] { y1, y2, y3, y4 };
			
			Path2D lanePath = new Path2D.Double();
			lanePath.moveTo(pointsX[3], pointsY[3]);
			for (int j = 0; j < pointsY.length; j++) {
				lanePath.lineTo(pointsX[j], pointsY[j]);
			}

			g.fill(lanePath);
			this.laneGeometry.put(lanes[i], lanePath);
		}

		this.drawTrafficLights(cr, endPoints, g);
	}

	/**
	 * Vykresli semafory pro zadanou krizovatku
	 * 
	 * @param cr   Instance krizovatky
	 * @param ends Seznam bodu vchazejicich do krizovatky
	 * @param g    graficky kontext
	 */
	void drawTrafficLights(CrossRoad cr, List<Point2D> ends, Graphics2D g) {
		final int TRAFFIC_LIGHT_WIDTH = 5;
		final double TRAFFIC_LIGHT_HEIGHT = 2.75 * TRAFFIC_LIGHT_WIDTH;
		final int TRAFFIC_LIGHT_OFFSET = 1;
		RoadSegment[] roads = cr.getRoads();
		for (int i = 0; i < roads.length; i++) {
			TrafficLight[] lights = cr.getTrafficLights(Direction.values()[i]);

			RoadSegment road = roads[i];
			Point2D point;
			double angle;

			if (road != null) {				
				if (ends.contains(road.getStartPosition())) {
					point = this.model2window(road.getStartPosition());
					angle = this.getAngle(this.getHeadingVector(road.getStartPosition(), road.getEndPosition()));
				} else {
					point = this.model2window(road.getEndPosition());
					angle = this.getAngle(this.getHeadingVector(road.getEndPosition(), road.getStartPosition()));
				}
				
				
				double minOffset = (road.getLaneWidth() / 2) + (road.getForwardLanesCount() - 1) * road.getLaneWidth() + 1;
				double tempOffset = road.getLaneSeparatorWidth() + road.getLaneWidth() * road.getBackwardLanesCount();
				double tempOffset2 = road.getLaneSeparatorWidth() + road.getLaneWidth() * (road.getBackwardLanesCount() + 1);
				
				AffineTransform tr = g.getTransform();

				g.translate(point.getX(), point.getY());
				g.rotate(angle );

				if (road.getBackwardLanesCount() != 0) {
					for (Lane lane : this.laneGeometry.keySet()) {
						Path2D geom = this.laneGeometry.get(lane);
						Point2D ptDst1 = null;
						Point2D ptDst2 = null;
						ptDst1 = g.getTransform().transform(new Point2D.Double(this.model2window(tempOffset), 0), ptDst1);
						if (geom.contains(ptDst1)) {
							ptDst2 = g.getTransform().transform(new Point2D.Double(this.model2window(tempOffset2), 0), ptDst2);
							if (!geom.contains(ptDst2)) {
								minOffset = (road.getLaneWidth() / 2) + (road.getBackwardLanesCount()) * road.getLaneWidth() + road.getLaneSeparatorWidth() + 1;
							}
						}
					}
				}
				
				int drawnLights = 0;
				for (int j = 3; j >= 1; j--) {
					if (lights[j] == null) continue;

					
					double xCoord = minOffset + ((TRAFFIC_LIGHT_WIDTH + TRAFFIC_LIGHT_OFFSET) * drawnLights);

					BufferedImage image = this.trafficLightImageRepository.get(Direction.values()[j])
							.get(lights[j].getCurrentState());
					Rectangle2D imageRect = new Rectangle2D.Double(this.model2window(xCoord), 0,
							this.model2window(TRAFFIC_LIGHT_WIDTH), this.model2window(TRAFFIC_LIGHT_HEIGHT));

					this.trafficLightGeometry.put(lights[j], imageRect);
					this.trafficLightGeometryTransform.put(lights[j], g.getTransform());
					this.trafficLightCrossroad.put(lights[j], cr);

					g.drawImage(image, (int) this.model2window(xCoord), 0, (int) this.model2window(TRAFFIC_LIGHT_WIDTH),
							(int) this.model2window(TRAFFIC_LIGHT_HEIGHT), null);
					
					drawnLights++;
				}

				g.setTransform(tr);
			}
		}
	}

	/**
	 * Ziska barvu vozidla podle jeho aktuani rychlosti (Bila = nejvyssi rychlost,
	 * Cerna = nejpomalejsi)
	 * 
	 * @param car Instance vozidla
	 * @return Instance tridy Color obsahujici barvu
	 */
	Color getCarColor(Car car) {
		if (clickedCars.contains(car))
			return Color.RED;

		int power = (int) ((car.getCurrentSpeed() / this.maxSpeedLimit) * 255);

		if (power > 255)
			power = 255;

		return new Color(power, power, power);
	}

	/**
	 * Ziska barvu jizdniho pruhu podle aktualniho modu vykreslovani barvy (Pocet
	 * vozidel nebo prumerna rychlost v jizdnim pruhu)
	 * 
	 * @param lane Instance jizdniho pruhu pro ktery je barva ziskavana
	 * @return Instance tridy Color obsahujici barvu jizdniho pruhu
	 */
	Color getLaneColor(Lane lane) {
		double power = 0;
		if (this.coloringMode == Coloring.SPEED) {
			double laneSpeedLimit = lane.getSpeedLimit();
			if (this.maxSpeedLimit < laneSpeedLimit)
				this.maxSpeedLimit = laneSpeedLimit;
			power = lane.getSpeedAverage() / laneSpeedLimit;
		} else {
			double laneCarCount = lane.getNumberOfCarsCurrent();
			if (this.maxCarCount < laneCarCount)
				this.maxCarCount = laneCarCount;
			power = laneCarCount / this.maxCarCount;
			power = (power * -1) + 1;
		}
		if (power > 1)
			power = 1;
		double hue = power * 0.35;
		return Color.getHSBColor((float) hue, 0.9f, 0.9f);
	}

	/**
	 * Vykresli vsechny casti dopravni site vcetne silnic a vozidel, graficke
	 * objekty vozidel ulozi do seznamu carGeometry
	 * 
	 * @param sim Instance simulatoru
	 * @param g   graficky kontext
	 */
	void drawTrafficState(Simulator sim, Graphics2D g) {
		double roadWidth = Integer.MAX_VALUE;

		for (RoadSegment segment : sim.getRoadSegments()) {
			roadWidth = Math.min(roadWidth, segment.getLaneWidth());
			drawRoadSegment(segment, g);
		}

		for (CrossRoad crossroad : sim.getCrossroads()) {
			drawCrossRoad(crossroad, g);
		}

		for (Car car : sim.getCars()) {
			Shape carShape = this.drawCar(car, this.model2window(roadWidth - 1), g);

			carGeometry.put(car, carShape);
			carGeometryTransform.put(car, g.getTransform());

			g.setTransform(defaultTransform);
		}
	}

	/**
	 * Posune animaci o zadany pocet milisekund sekundy
	 * 
	 * @param animationStep pocet milisekund o ktere se posnue animace
	 */
	void nextStep(double animationStep) {
		this.simulationTime += animationStep;
		this.sim.nextStep(animationStep / 1000.0);
	}

	/**
	 * Funkce zajisti vykresleni dopravy v zadanem grafickem kontextu
	 * 
	 * @param g      Graficky kontext
	 * @param width  Sirka platna
	 * @param height Vyska platna
	 */
	public void paintTraffic(Graphics2D g, double width, double height) {
		this.graphicsHeight = height;
		this.graphicsWidth = width;

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		this.defaultTransform = g.getTransform();

		this.computeModelDimensions();
		this.computeModel2WindowTransformation(width, height);

		this.drawTrafficState(sim, g);
	}

	// STATISTICKE FUNKCE
	/**
	 * Sbira statisticka data pro veskere jizdni pruhy a automobily
	 */
	void gatherStatData() {
		for (Lane lane : laneGeometry.keySet().toArray(new Lane[0])) {
			int carsCurrent = lane.getNumberOfCarsCurrent();
			int carsTotal = lane.getNumberOfCarsTotal();

			if (this.passedCarsCurrent.get(lane) == null) {
				this.passedCarsCurrent.put(lane, new ArrayList<>());
			}
			if (this.passedCarsTotal.get(lane) == null) {
				this.passedCarsTotal.put(lane, new ArrayList<>());
			}
			if (this.passedCarsTime.get(lane) == null) {
				this.passedCarsTime.put(lane, new ArrayList<>());
			}

			this.passedCarsCurrent.get(lane).add(carsCurrent);
			this.passedCarsTotal.get(lane).add(carsTotal);
			this.passedCarsTime.get(lane).add(this.simulationTime);
		}

		for (Car car : carGeometry.keySet().toArray(new Car[0])) {
			double carSpeed = car.getCurrentSpeed();

			if (this.carSpeedTime.get(car) == null) {
				this.carSpeedTime.put(car, new ArrayList<>());
			}

			if (this.carSpeed.get(car) == null) {
				this.carSpeed.put(car, new ArrayList<>());
			}

			this.carSpeedTime.get(car).add(this.simulationTime);
			this.carSpeed.get(car).add(carSpeed);

		}
	}

	// FUNKCE ZOOM A PAN
	/**
	 * Funkce zajistujici priblizeni
	 */
	public void zoomIn() {
		this.zoomScale += 1;

		this.panX += this.graphicsWidth / (4 * (zoomScales[zoomScale - 2]));
		this.panY += this.graphicsHeight / (4 * (zoomScales[zoomScale - 2]));
		
		this.repaint();
	}

	/**
	 * Funkce zajistujici oddaleni
	 */
	public void zoomOut() {
		this.zoomScale -= 1;

		this.panX -= this.graphicsWidth / (4 * (zoomScales[zoomScale - 1]));
		this.panY -= this.graphicsHeight / (4 * (zoomScales[zoomScale - 1]));
		
		this.repaint();
	}

	/**
	 * Funkce zajistujici vyrovnani na stred obrazovky
	 */
	public void centerPan() {
		this.panX = 0;
		this.panY = 0;
		this.newPanX = 0;
		this.newPanY = 0;

		if (this.zoomScale == 1) {
			this.panX -= this.graphicsWidth / 4 / (zoomScales[0]);
			this.panY -= this.graphicsHeight / 4 / (zoomScales[0]);
		}

		for (int i = 2; i < this.zoomScale; i++) {
			this.panX += this.graphicsWidth / (4 * zoomScales[i - 1]);
			this.panY += this.graphicsHeight / (4 * zoomScales[i - 1]);
		}
		
		this.repaint();
	}

	// GETTERY A SETTERY
	/**
	 * Nastavi instanci Simulator inicializovanou v main
	 * 
	 * @param sim instance tridy Simulator
	 */
	public void setSim(Simulator sim) {
		this.sim = sim;
	}

	/**
	 * Nastavi scenar vygenerovany v main
	 * 
	 * @param scenario Scenar
	 */
	public void setScenario(String scenario) {
		sim.runScenario(scenario);
	}

	/**
	 * Ziska nasobek priblizeni
	 * 
	 * @return nasobek priblizeni
	 */
	public int getZoomScale() {
		return zoomScale;
	}

	/**
	 * Vrati aktualni mod vybarvovani silnice
	 * 
	 * @return aktualni mod vybarvovani silnice
	 */
	public Coloring getColoringMode() {
		return coloringMode;
	}

	/**
	 * Nastavi novy mod vybarvovani silnice
	 * 
	 * @param mode Mod vybarvovani silnice
	 */
	public void setColoringMode(Coloring mode) {
		this.coloringMode = mode;
		this.repaint();
	}

	// OVERRIDE

	/**
	 * Zajistuje vykresleni prvku simulace na platno a sbirani dat
	 * 
	 * @param g graficky kontext
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		paintTraffic(g2, this.getWidth(), this.getHeight());

		if (!setMaxValues) {
			setMaxValues = true;

			this.maxX = model2window(this.scenarioMaxX);
			this.maxY = model2window(this.scenarioMaxY);
			this.minX = model2window(this.scenarioMinX);
			this.minY = model2window(this.scenarioMinY);
		}

		if (this.simulationTime - this.meassurementPrevStep > 500) {
			gatherStatData();
			this.meassurementPrevStep = this.simulationTime;
		}
	}

	/**
	 * Funkce zajistujici vytisknuti na papir
	 */
	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
		if (pageIndex > 0) {
			return NO_SUCH_PAGE;
		}

		Graphics2D g2 = (Graphics2D) graphics;

		g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		double scale = Math.min(pageFormat.getImageableWidth() / this.getWidth(),
				pageFormat.getImageableHeight() / this.getHeight());

		g2.scale(scale, scale);
		paintTraffic(g2, this.getWidth(), this.getHeight());

		return PAGE_EXISTS;
	}
}
