package TrafficSimulator;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import TrafficSim.CrossRoad;
import TrafficSim.Direction;
import TrafficSim.RoadSegment;
import TrafficSim.TrafficLight;
import TrafficSim.TrafficLightState;

/**
 * Trida zajisti vykresleni casoveho planu krizovatky
 * 
 * @author Tomas Linhart
 * @version 1.0
 *
 */
public class TrafficTimeTable extends JPanel {
	private static final long serialVersionUID = 1L;
	
	/** Zvoleny semafor */
	private final TrafficLight light;
	
	/** Instance krizovatky, pro kterou je plan vykreslen */
	private final CrossRoad cr;

	/** Velikost ctverce planu */
	private final int SQUARE_SIZE = 14;

	/**
	 * Konstruktor, nacte parametry 
	 * @param light Vybrany semafor
	 * @param cr Instance krizovatky
	 */
	TrafficTimeTable(TrafficLight light, CrossRoad cr) {
		this.light = light;
		this.cr = cr;
	}

	/**
	 * Zajistuje vykresleni signalniho planu
	 */
	@Override
	protected void paintComponent(Graphics g1) {
		super.paintComponent(g1);

		int seconds = light.getTimeTable().length;

		Graphics2D g = (Graphics2D) g1;

		g.translate(SQUARE_SIZE * 2, SQUARE_SIZE * 2);
		this.drawAxis(seconds, g);

		g.translate(0, SQUARE_SIZE * 2);

		this.drawLanes(g);
	}

	/**
	 * Vykresli casovou osu
	 * 
	 * @param seconds Celkovy pocet sekund
	 * @param g graficky kontext
	 */
	private void drawAxis(int seconds, Graphics2D g) {
		g.drawLine(0, 0, SQUARE_SIZE * seconds, 0);
		for (int i = 0; i <= seconds; i++) {
			g.drawLine(i * SQUARE_SIZE, -1 * SQUARE_SIZE / 2, i * SQUARE_SIZE, SQUARE_SIZE / 2);

			if (i % 5 == 0) {
				g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 9));

				String text = ((Integer) i).toString();
				FontMetrics metrics = g.getFontMetrics(g.getFont());
				int textWidth = metrics.stringWidth(text);

				g.drawString(text, i * SQUARE_SIZE - textWidth, -1 * SQUARE_SIZE / 2);
			}
		}
	}

	/**
	 * Vykresli vsechny casti casoveho planu
	 * 
	 * @param g graficky kontext
	 */
	private void drawLanes(Graphics2D g) {
		RoadSegment[] roads = cr.getRoads();
		for (int i = 0; i < roads.length; i++) {
			TrafficLight[] lights = cr.getTrafficLights(Direction.values()[i]);
			for (TrafficLight currLight : lights) {
				if (currLight == null)
					continue;
				

				TrafficLightState[] timeTable = currLight.getTimeTable();
				if (currLight == this.light) {
					g.setColor(Color.BLACK);
					g.drawRect(-2, (-1 * SQUARE_SIZE / 2) -2, timeTable.length * SQUARE_SIZE + 4, SQUARE_SIZE + 4);
				}
				for (int j = 0; j < timeTable.length; j++) {
					this.drawLightRectangle(g, timeTable[j], j * SQUARE_SIZE, -1 * SQUARE_SIZE / 2);
				}

				g.translate(0, 2 * SQUARE_SIZE);
			}
		}
	}

	/**
	 * Vykresli jeden ctverecek casoveho planu
	 * @param g graficky kontext
	 * @param state Stav semafor v danou sekundu
	 * @param x souradnice x
	 * @param y souradnice y
	 */
	private void drawLightRectangle(Graphics2D g, TrafficLightState state, int x, int y) {
		if (state == TrafficLightState.PREPARE_TO_GO) {
			g.setColor(Color.RED);
			g.fillRect(x, y, SQUARE_SIZE, SQUARE_SIZE / 2);
			g.setColor(Color.YELLOW);
			g.fillRect(x, y + SQUARE_SIZE / 2, SQUARE_SIZE, SQUARE_SIZE / 2);
		} else {
			if (state == TrafficLightState.GO) {
				g.setColor(Color.GREEN);
			} else if (state == TrafficLightState.PREPARE_TO_STOP) {
				g.setColor(Color.YELLOW);
			} else if (state == TrafficLightState.STOP) {
				g.setColor(Color.RED);
			} else {
				g.setColor(Color.GRAY);
			}
			g.fillRect(x, y, SQUARE_SIZE, SQUARE_SIZE);
		}
	}
	
	/**
	 * Vypocte sirku planu v px
	 * @return sirka planu v px
	 */
	public int calculateWidth() {
		int seconds = light.getTimeTable().length;
		return SQUARE_SIZE * (seconds + 4);
	}
	
	/**
	 * Vypocte vysku planu v px
	 * @return vyska planu v px
	 */
	public int calculateHeight() {
		int size = 4 * SQUARE_SIZE;
		RoadSegment[] roads = cr.getRoads();
		for (int i = 0; i < roads.length; i++) {
			TrafficLight[] lights = cr.getTrafficLights(Direction.values()[i]);
			for (TrafficLight light : lights) {
				if (light == null)
					continue;

				size += 2 * SQUARE_SIZE;
			}
		}
		return size;
	}
}
